<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return view('index');
});
$router->post('/convert','ConvertController@convert');
// errors
$router->get('/400', function () {
    return view('error',["error"=>400]);
});
$router->get('/401', function () {
    return view('error',["error"=>401]);
});
$router->get('/403', function () {
    return view('error',["error"=>403]);
});
$router->get('/404', function () {
    return view('error',["error"=>404]);
});
$router->get('/500', function () {
    return view('error',["error"=>500]);
});
$router->get('/503', function () {
    return view('error',["error"=>503]);
});

