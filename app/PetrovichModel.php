<?php

namespace App;

// use Illuminate\Auth\Authenticatable;
// use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
// use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
// use Laravel\Lumen\Auth\Authorizable;
use Petrovich;

class PetrovichModel extends Model
{
    // use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lastname','firstname','middlename'
    ];
    public function convert($data)
    {

        // return $data;
        $last=$data['lastname'];
        if ($last == '') $last=" ";
        $first=$data['firstname'];
        if ($first == '') $first=" ";
        $middle=$data['middlename'];
        if ($middle == '') $middle=" ";

        mb_internal_encoding('UTF-8');

        $petrovich = new Petrovich();

        // dd($petrovich->detectGender($middle));
        if (is_null($middle) || $middle==''){
            $gender=0;
        } else {
            $gender=$petrovich->detectGender($middle);

        }
        $petrovich = new Petrovich($gender);

        $result[]=$petrovich->lastname($last, Petrovich::CASE_NOMENATIVE)." ".$petrovich->firstname($first, Petrovich::CASE_NOMENATIVE)." ".$petrovich->middlename($middle, Petrovich::CASE_NOMENATIVE);
        $result[]=$petrovich->lastname($last, Petrovich::CASE_GENITIVE)." ".$petrovich->firstname($first, Petrovich::CASE_GENITIVE)." ".$petrovich->middlename($middle, Petrovich::CASE_GENITIVE);
        $result[]=$petrovich->lastname($last, Petrovich::CASE_DATIVE)." ".$petrovich->firstname($first, Petrovich::CASE_DATIVE)." ".$petrovich->middlename($middle, Petrovich::CASE_DATIVE);
        $result[]=$petrovich->lastname($last, Petrovich::CASE_ACCUSATIVE)." ".$petrovich->firstname($first, Petrovich::CASE_ACCUSATIVE)." ".$petrovich->middlename($middle, Petrovich::CASE_ACCUSATIVE);
        $result[]=$petrovich->lastname($last, Petrovich::CASE_INSTRUMENTAL)." ".$petrovich->firstname($first, Petrovich::CASE_INSTRUMENTAL)." ".$petrovich->middlename($middle, Petrovich::CASE_INSTRUMENTAL);
        $result[]=$petrovich->lastname($last, Petrovich::CASE_PREPOSITIONAL)." ".$petrovich->firstname($first, Petrovich::CASE_PREPOSITIONAL)." ".$petrovich->middlename($middle, Petrovich::CASE_PREPOSITIONAL);

        return $result;
    }

}
