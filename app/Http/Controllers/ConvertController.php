<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PetrovichModel;

class ConvertController extends Controller
{
    public function __construct()
    {
        //
    }
    public function convert(Request $request){
        // dd($request);
        $petrovich=new PetrovichModel();
        $data=array(
            'lastname' => $request->input('lastname'),
            'firstname' => $request->input('firstname'),
            'middlename' => $request->input('middlename')
        );
        // return $request;
        return $petrovich->convert($data);
    }
    //
}
