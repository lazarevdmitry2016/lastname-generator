<?php  if (env('GOOGLE_ANALYTICS') !== '') { ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-<?php echo env('GOOGLE_ANALYTICS'); ?>-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-<?php echo env('GOOGLE_ANALYTICS'); ?>-1');
</script>
<?php  } ?>
