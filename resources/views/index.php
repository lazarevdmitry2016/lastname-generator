<!DOCTYPE html>
<html lang="ru" prefix="og: //ogp.me/ns#">

<head >
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Генератор Фамилий, Имен и Отчеств" >
  <link rel="icon" type="image/png" href="img/favicon.ico" />

  <title>Генератор Фамилий, Имен и Отчеств</title>
  <!-- OpenGraph -->
  <meta property="og:title" content="Генератор Фамилий, Имен и Отчеств" />
  <meta property="og:description" content="Данное приложение позволяет вывести фамилию, имя и отчество во всех 6 падежах." />
  <meta property="og:locale" content="ru_RU" />
  <!-- /OpenGraph -->
  <?php require_once('partials/head.php'); ?>

</head>

<body itemscope itemtype="http://schema.org/WebSite">
    <?php require_once('partials/yandex-metrika.php'); ?>
    <?php require_once('partials/mailru-rating.php'); ?>
    <?php require_once('partials/google-analytics.php'); ?>
  <header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
							<span class="sr-only">Переключить навигацию</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
            <div class="navbar-brand">
              <a href="/" data-tooltip="Главная">
                  <img src="img/logo.png" alt="Генератор Фамилий, Имен и Отчеств" style="max-height: 100%">
              </a>
            </div>
          </div>

          <div class="navbar-collapse collapse">
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                  <li>

                  </li>
                  <li><a href="#" class="fb tool-tip" data-tooltip="Facebook"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#" class="twitter tool-tip" data-tooltip="Twitter"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#" class="ytube tool-tip" data-tooltip="You Tube"><i class="fa fa-youtube-play"></i></a></li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    </nav>
  </header>

  <div id="home">

    <div class="slider">
      <div></div>
    </div>
  </div>

  <section id="about">
    <div class="container">
      <div class="center">
        <div class="col-md-6 col-md-offset-3">
          <h2 itemprop="name">Генератор Фамилий, Имен и Отчеств</h2>
          <hr>
          <p class="lead" itemprop="about">Данное приложение позволяет вывести фамилию, имя и отчество во всех 6 падежах.<br />Чтобы поделиться ссылкой на эту страницу с друзьями, нажмите на одну из кнопок с логотипами социальных сетей вверху страницы.</p>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-2">

        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-8 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div id="errormessage">
            </div>

          <form class="jumbotron" id="submitForm">
              <div class="form-group">
                  <input required type="text" name="lastname" value="" placeholder="Фамилия" class="form-control">
              </div>
              <div class="form-group">
                  <input required type="text" name="firstname" value="" placeholder="Имя" class="form-control">
              </div>
              <div class="form-group">
                  <input required type="text" name="middlename" value="" placeholder="Отчество" class="form-control">
              </div>
              <div class="form-group">
                  <button id="submit" class="form-control btn btn-default">Сгенерировать
                  </button>
              </div>
          </form>
          <div id="sendmessage" >
              <textarea id="message"rows="7" style="width:100%"></textarea>
              <button id="clipboardBtn" data-clipboard-target="#message">
                  Скопировать в буфер обмена
              </button>
          </div>
        </div>
        <!--/.col-sm-6-->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-2">

        </div>
      </div>
      <!--/.row-->
    </div>
    <!--/.container-->
  </section>
  <!--/#about-->

  <div class="sub-footer">
    <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
          <div class="copyright text-left">
              &copy; <span itemprop="copyrightHolder">Anyar Theme</span>. Все права защищены.
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
          <div class="copyright text-center" >
            Разработка:  <a href="https://lazarev-dmitry.ru/" target="_blank" itemprop="author">Дмитрий Лазарев</a>
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> 
        <div class="copyright text-right">
          <div class="credits">
              <!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Anyar
              -->
              Дизайн: <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/js/bootstrap.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/1.5.25/jquery.isotope.min.js"></script>
  <script src="js/functions.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
  <script>
      $(document).ready(function(){
          $("#submit").click(function(event){
              event.preventDefault();
              $("#sendmessage").removeClass("show");
              $("#errormessage").removeClass("show");
            var data={
                'lastname':$("input[name='lastname']").val(),
                'firstname':$("input[name='firstname']").val(),
                'middlename':$("input[name='middlename']").val()
            };
              $.ajax({
                  url:"/convert",
                  method:"POST",
                  data:data,
                  beforeSend:function(j,s){

                  },
                  error:function(j,t,e){
                      $("#errormessage").text(j.responseJSON.message);
                      $("#errormessage").addClass("show");
                  },
                  success:function(d,t,j){
                      var txt="";
                      for (var i=0;i<d.length;i++){
                          txt+=d[i]+"\n";
                      }

                      $("#message").text(txt);
                      $("#sendmessage").addClass("show");
                      $("#sendmessage").addClass("animate bounceInDown");
                      new ClipboardJS('#clipboardBtn');
                  }
              });
          });
      });
  </script>
</body>

</html>
